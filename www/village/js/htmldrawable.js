var World = {
	
	init: function initFn() {
		this.createOverlays();
		document.getElementById('close-overlay').addEventListener("click", function(){ document.getElementById('content-overlay').style.display = 'none' });
	},
	
	createButton: function createButtonFn(content) {
		var pulsatingButton = new AR.HtmlDrawable({uri: "./button.html"}, 1, {
			onClick : function() {
				document.getElementById('overlay-title').innerHTML = content.title;
				document.getElementById('overlay-lead').innerHTML = content.lead;
				document.getElementById("overlay-media").innerHTML = content.media ? content.media : '';
				document.getElementById("overlay-text").innerHTML = content.text ? content.text : '';
				document.getElementById("overlay-link").innerHTML = content.link ? '' : '';
				// LINK is deactivated because the webview does not open properly. Can be shown for prototype reasons
				// document.getElementById("overlay-link").innerHTML = content.link ? content.link : '';
				// document.getElementById("slick").slick()
				// $('#slick').append('<img src="https://via.placeholder.com/1600x900/09f/fff.png%20C/O%20https://placeholder.com/" width="100%" />').slick()
				document.getElementById('content-overlay').style.display = 'block';
			},
			horizontalAnchor : AR.CONST.HORIZONTAL_ANCHOR.LEFT,
			verticalAnchor: AR.CONST.VERTICAL_ANCHOR.TOP,
			opacity : 1,
			translate: {
				x: -0.25,
				y: 0
			}
		});
		return pulsatingButton
	},
	
	createOverlays: function createOverlaysFn() {
		
		this.targetCollectionResource = new AR.TargetCollectionResource("assets/tracker.wtc", {
			onError: World.onError
		});
		
		var tracker = new AR.ImageTracker(this.targetCollectionResource, {
			onTargetsLoaded: World.showInfoBar,
			onError: World.onError,
			maximumNumberOfConcurrentlyTrackableTargets: 5
		});
		
		// var churchImages = '<img src="./assets/gallery/05_kirche_einweihung.jpg" />';
		
		var buttonContent = [
			{
				title: 'Evangelische Kirche',
				lead: 'Erbaut im Jahre 1954',
				media: '<iframe src="https://albumizr.com/a/dA3t" scrolling="no" frameborder="0" allowfullscreen width="700" height="400"></iframe>',
				text: '<p>Unsere Gemeinde zählt rund 700 Mitglieder. Der grösste Teil lebt in Lenzerheide - Valbella und einige Dutzend wohnen in den Dörfern Lain, Muldain und Zorten (Vaz/Obervaz). Zur Kirchgemeinde gehören auch die Reformierten von Lantsch/Lenz.</p><p>Seit 1954 verfügen wir über ein schmuckes Bergkirchlein. Hundert Meter weiter südlich steht unser geräumiges Kirchgemeindehaus, in dem verschiedene Gruppen zu Gast sind. Unsere Gemeinde wird von einem Pfarrehepaar betreut. Sie stehen für vertrauliche Gespräche bzw. Seelsorge stets zur Verfügung.</p><p>Da wir hier in der kirchlichen Diaspora leben, d.h. die Mehrheit der Einwohnerinnen und Einwohner katholisch sind, gibt es viele gemischt konfessionelle Ehen. Ein guter geschwisterlicher ökumenischer Kontakt zur katholischen Pfarrei ist für uns selbstverständlich.</p>',
				link: '<a href="https://www.lenzerheide.ch" target="_blank"><i class="icon external link"></i>Mehr lesen</a>'
			},
			{
				title: 'Erster Skitransport',
				lead: 'Der erste Skitransport erfolgte im Jahre 1937 und wurde in der ganzen Schweiz diskutiert',
				media: '<img src="./assets/gallery/skitransport.jpg" width="100%" />',
				text: '<p>Unsere Gemeinde zählt rund 700 Mitglieder. Der grösste Teil lebt in Lenzerheide - Valbella und einige Dutzend wohnen in den Dörfern Lain, Muldain und Zorten (Vaz/Obervaz). Zur Kirchgemeinde gehören auch die Reformierten von Lantsch/Lenz.</p><p>Seit 1954 verfügen wir über ein schmuckes Bergkirchlein. Hundert Meter weiter südlich steht unser geräumiges Kirchgemeindehaus, in dem verschiedene Gruppen zu Gast sind. Unsere Gemeinde wird von einem Pfarrehepaar betreut. Sie stehen für vertrauliche Gespräche bzw. Seelsorge stets zur Verfügung.</p><p>Da wir hier in der kirchlichen Diaspora leben, d.h. die Mehrheit der Einwohnerinnen und Einwohner katholisch sind, gibt es viele gemischt konfessionelle Ehen. Ein guter geschwisterlicher ökumenischer Kontakt zur katholischen Pfarrei ist für uns selbstverständlich.</p>',
				link: '<a href="www.lenzerheide.ch" target="_blank"><i class="icon external link"></i>Mehr lesen</a>'
			},
			{
				title: 'Der Dorfkern',
				lead: 'Im Tal zwischen hoch aufragenden Bergen befindet sich das idyllische Dorfzentrum',
				media: '<iframe width="560" height="315" src="https://www.youtube.com/embed/DtneMbaXf3E" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>',
				text: '<p>Unsere Gemeinde zählt rund 700 Mitglieder. Der grösste Teil lebt in Lenzerheide - Valbella und einige Dutzend wohnen in den Dörfern Lain, Muldain und Zorten (Vaz/Obervaz). Zur Kirchgemeinde gehören auch die Reformierten von Lantsch/Lenz.</p><p>Seit 1954 verfügen wir über ein schmuckes Bergkirchlein. Hundert Meter weiter südlich steht unser geräumiges Kirchgemeindehaus, in dem verschiedene Gruppen zu Gast sind. Unsere Gemeinde wird von einem Pfarrehepaar betreut. Sie stehen für vertrauliche Gespräche bzw. Seelsorge stets zur Verfügung.</p><p>Da wir hier in der kirchlichen Diaspora leben, d.h. die Mehrheit der Einwohnerinnen und Einwohner katholisch sind, gibt es viele gemischt konfessionelle Ehen. Ein guter geschwisterlicher ökumenischer Kontakt zur katholischen Pfarrei ist für uns selbstverständlich.</p>',
				link: '<a href="www.lenzerheide.ch" target="_blank"><i class="icon external link"></i>Mehr lesen</a>'
			}
		]
		
		var churchButtonElement = this.createButton(buttonContent[0]);
		var mountainButtonElement = this.createButton(buttonContent[1]);
		var villageButtonElement = this.createButton(buttonContent[2]);
		
		this.pageOne = new AR.ImageTrackable(tracker, "church_window", {
			drawables: {
				// cam: [overlayOne, pageOneButton, weatherWidget]
				cam: [churchButtonElement]
			},
			onImageRecognized: World.hideInfoBar,
			onError: World.onError
		});
		
		this.pageTwo = new AR.ImageTrackable(tracker, "peak1", {
			drawables: {
				// cam: [overlayOne, pageOneButton, weatherWidget]
				cam: [mountainButtonElement]
			},
			onImageRecognized: World.hideInfoBar,
			onError: World.onError
		});
		
		this.pageThree = new AR.ImageTrackable(tracker, "village_far1", {
			drawables: {
				// cam: [overlayOne, pageOneButton, weatherWidget]
				cam: [villageButtonElement]
			},
			onImageRecognized: World.hideInfoBar,
			onError: World.onError
		});
		
	},
	
	onError: function onErrorFn(error) {
		alert(error)
	},
	
	hideInfoBar: function hideInfoBarFn() {
		document.getElementById("infoBox").style.display = "none";
	},
	
	showInfoBar: function worldLoadedFn() {
		document.getElementById("infoBox").style.display = "table";
		document.getElementById("loadingMessage").style.display = "none";
	}
	
};

World.init();
